import download from './download.png';
import './App.css';

function App() {
  return (
    <div className="wrapper themeDarkGrey">
    {/* Boolean variable switches this class to set the color scheme */}
    <main className="site-content">
      <header className="container-fluid">
        <div id="headerBranding" className="row">
          <a href="https://beta-test.internnexus.com/#/" target="_blank">
            {/* Logo link token */}
            <img
                className="media"
                data-object-fit="cover"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAABICAYAAAAAoJJcAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABazSURBVHhe7Z0HkJVFnsB7xmGGESagIyYslRVLBCnzqWsZdtcA5lgebiGegTsDBgwYby1Mq4JCGQ7cXRNr9oycZbgzlKuWgVMMbJnOLCoszAzIMDC8619//X+v55t+YWbeN+8B/avqev31119//XX3v7v/nV6FJqUCgUDJqLS/gUCgRAQhDARKTBDCQKDEBCEMBEpMEMJAoMQEIQwESkwQwkCgxPTZPGEqlfs1Oh7WFgisW/SJEFZpAVypf5tG7h45WJZXD1T9VrSrFR/NU6tUi1oVBDGwDpK4EEoLOGjMWLVs/6NV+4rV5hqqaypVe1uFqvv5K1Xx9F2q5f8+UFXrKdWxOghjYN0hMZ2Q1g8DaQHUAqdSWsqs6d/WpquBDtW60ZYqddjJqp/2qy8DgXWKxIRQupYIVseeY6IWMCZhLao2EkhN6+ChqmGfQ023NZ/+GEgG0l0qT4xcr1cZ8iNJEhNCybjqnX+jWiqqjT0nqQ5VNXw3exHoa6TXUtm4uardegdj6rWpUvVqla47Q8WYHInohKYG1Q0cmbfJhKvUgi2G2zsa2/J1QbeSTSvaVfOU35vWUMcrcg/0CeRZ3bBRSh1zlmod2KjqU+3Gfb3WBWrpX6aplQu/D3mSEIm0hGQWAkhXdFlNXWfBo0saN5aFNdWqITaCGuhDRo9XrQM2NPmFqkAPZtlGW6mmA8daD5kWM1A8EuuOdqo1HUHLihHUapWqrY+uA32GtIKtg4fovHIqTJ0n6PIrf7Wjqh6wQaQn6tuha1pcEhPCTuico6sJdHO6GLVcNXYsVnVLfzJ+QDKajM9lBNce6D6r65rMlBG6eVoQ+dV5t7C6VuuH20RuGreCDQLZe3R6JjdPaGrYidOMvW3G+ea3EKhtaTzRDenS5gI/gL9ck/35Ckunlnsdg7TZcLffqfbfHq8qv/7UumpVotZaNLWvPqdaP51n0lnSPM66nIa9oU+EkG5Ow+Jv1bJbzs/aMxXBQ5AYWcU+cMxYtbphkPXhp7J5sVr8X/fnLAAigIceeqhaf/31jR369++vFixYoJ5//nlz3ZtClE3I14SCKUJYcfB41fboH9WK+R8Yd7cSxM5gW8Ovj4kcYrS++5JqX/aPIIg9QKdZckJIF7G/EcKh5rrupy/Uqhl/8C5RE8ETIWRgp27yrGigwEiuf5qjYfEX6pepUSubrSUUAfnyyy/VlltuaezCu+++q3bddVdjT0IIodwLJnEfsPNv1LKj/9WoBx2P/0W1z/2f6J70TG3eDLx4ppMnUK2fWaqWT78ojKD2kMR0wnihJKNoEavHTohq1dh9hE4EENLd0HRma50SfcU1DhSWXIIA7e2RXuric+suvHfkyJHqk08+6WQef/xx62PNgRHR2kP+RQ0Ysbd1iZYRduqCWl0xLaGaiuVt1hboLn0zMKMxq2M0zcP3UE0nnm8y1RWavDWok+HlyrBhwzqZLbbYwt5Zc6AlZKqoSreK/YeNMpVjF2IVYKB39JkQGhAk3bIt1ILY8M9nGidaxHwtWKCP0PkjleXCmoGq8vDx6akJoaZfRqcOFIe+FUJBZ3bziH3Nwm4go8sBKoNCjItct7EYPQu5ns1mXHz3i2GyQmWpW7vmQUNUzSmXGzUB9UBUhULwvU9MHJ8fMYLvXj6T6zkX332fSQrdC0xmYIZIk3HpgZl4F0a3iHR9Km67WjUv+Lt1jEbgAFWwPwMzAxsjB193VOsmMjDDbbpOvm6tJCB6Gt1ElzfeeEPttddeput48MEHW1c/n332mXrppZfsVYQ819TUpK699lrrGvHpp5+qyy+/XG200Ubq448/7vIsXHLJJWrEiBHG3tLSol5++WX18MMPm2uX448/XjU0NNgr3a1vbk77k3t33nmn2n///dX222+fVdddsWKFev311823AOlF+piBmaOi3kkaq4/X/fStGVBbrtvJ9GCZoPMAfb/t+olmdNRljz32UMcdd5zaeOONzfWPP/6oHnnkEfXmm2+aa3k394899thOcXa/D+LfP3DgQPX000+rzz//XJ166qnWNYJvvPfee419m222UQcddFCnsH/44Qf1zDPP2KsI4rrDDjvYq85UV1en889XvnqLDrNEQgg6A5mgb/+Pq9TqJd91GpjpayE88MAD1XPPPWdds/P9998bYbvtttvMNdMeFIZ83H333erkk0+2V0pNnTpVnX++f+70q6++UieccEK6sEI87gg4z991112mAgC+ffbs2erEE08017lgVPjCCy9MVwxeIXRgz+fKGdeqfpOv1XkyOJOfHiGkIn3xv19W++67r7mO88ILL5j0dvGNXCMUH374oREQ8smFNNpqq62MAGOvqamxd3RXeuFCU/HBuHHj1D333GPsguQ5EDYCGy8XcST/khDC0nRHBZ2RCFn/UyaZ1fvoHqXa0LtyZafxv6xsttlm6tZbbzUtWHdYvny5tSk1ffr0rAIIFMZnn32208BOvLtLIXv00UfTAkjtD4V+xy677GLeQSEsBCrSVaecHOmEvgrVYe7/zssqgHDAAQeYFt/lyiuvtLYMU6ZMMb9nnXWW+XW55pprrK1z2sKyZcusza8mSKtI+pIG+QQQ4u8oJqUVQtvdQfeoHjdJVWjdUFqtcueiiy6ytsKorY0GPCj0EydONPZcNDY2qvvuu89edYX7bu3fE3h+2rRoRVNObD5VbvdPZsAmF1Qw2bp1LgjpeeedZ6+UaY1onV1Gjx5tpn7oqrp88MEHpuvdW2bMmGHSsRAk/5KgxC2h7reYbma1qWk3+P05pgtr87zkLFmyRL3yyism0+OQeehfdH3o3sQLEMjz3KM7CRdffLH5daGLSxjSmgkUVAphIeQSSLquxIP3xCmkFYjyqcOejBDLHH3N3GJFv6iQjh2b2XEhkH6+NHS753DppZdaWwTfhIoQ/7arr77a2nrHTjvtZG0ZyCvywjVu/iVBaYVQMBnbbvYdbjjhqsgtAXKNYPpA6d9vv/3UqFGjTCGOwwAIehv6xSGHHGJdM8yfP988z4qc6667zrjtvnvnrVoI6uabb27COPvss61rhsMOO8zasiPCng0GOIgH78Gvi3RnewMDbJVLWk0rHw8P/Y/0wzzxxBPWNYIWUwZtgOWD+Heh+++CUPgGrnpCv37pJSEGKinyirxwjZt/SVAeQijoGnfBkJGq4ehzrENpYVRMeP/9960tw6abbmptkY4WR54XZZ6Runihevvtt61NmS4WLavLtttua21+GDAYNGiQEbJsuF2pb775xtoyNNUPsLZuYrsstISMnNIziPPkk09am1IPPPCAtWXYbbfoNAVJozPOOMP8ZuOyyy4zv8UYIInrz+TN4sWL1c8//5w2VAzSG0liUAbKRwhNtzRi4c6/VvX7+BcKl4p4rQnd1RO22247a8uwzz77mBYaXZjfuro6eycil84SH3EthG4t00PIxORAtqnJVIsLo8B8F0amDVzcigyYOrnjjjvsVWdoSX3TPD3F18UkvWnNxTCIRFeaKRLyKIkxi/JqCcFkeLXq+M3osl+d0d0RM3cHh4C+IzqPay8ECjhQQydVS2f09vwMGNC1RZVv6s63ocP6+Oijj6ytONx0003Wlp+bb77Z2opP+QmhZUlVvVnDWGgBWFvxFew+Q6d93bJFavDnf3daxWTyQ1oY5hh90xVwwQUX9Lzr7IGu5kknndRFT/ZBV5U5xyQoPyE0Na/ObLcGXsMFUQqYL7PRARmM8BkGW9566y3rswRogata3aJa7/6javgY3ZVKMXvX1Pd9tGq+b8Pwfaxecbn7ntlZu+C0pHfq+5CrW+iurMmHmajfcoiZi2S6hmkLfn2jufn0855Sti3h2gCT7ij1GEYBv/jiC3snQ1VVlZm4ZwXJmNEZM/bYo9TvfrufWfZWSlLrN5r9n7/cf6MWxL9FrWEWfDoWI47xb5PvYzDJXT6Gv3yrfY488kijnwk//ZQ5EkVAiJnGIM1zCSST9eRN4+BN1VNPPaVuuOEGsxpq0qRJat68edZXhqTmCoMQFglG0uLzfCj2Mkd2/fXXm0GH+FwdBYb71OwrV2XMz81L1Weff2l9lQjd6nW0rTZnj0KzFsRNvpmvqvvrVsjq7tCuf2q1nxefn2OuXZjrjH+bfN/tt99ufUWwEimOTz9EWATCXrVqlb3KwCgqpybMmjXLunSFVo+05x1ff/218Y8hTF9lEB+5LhZBCIsEi5Pnzp1rr7oigzgsNVtjsK1eW0Wr+WV8eMHMf1f1771urpnbFToGVKm3352XdVDFhztFQMsVXzhApUYXMN7NpYfhTti/9tpr1pYcxIV1uUkQhLAIyMjk6aef3qU1jHPuued6V66UOyIuCOLCv05Tgz9D2HQTGBuo6Un3mcGWM8/sunicdZ0Qn+QHBmlkbS3LAJNOU0ZSfXOsxWCdEkIOdoojE+q+eUDXv08fcN0QRFb8s87R1xqIX7o6bK3xKf5x3Dj54u7D9x3onYK7AEFYr9ZZD9pJqDr7ZZeL7C1c8sjUSEfUrWG1M/XIahYGOQrpuklcH31qjncw5oorrjC/MhXjwiDNfXZtLcLBap1cq4ZcJA0KGXnmO1hOJ5VLElNBOswSbmXqLbrAdGcrE0PMG27o7IXTfPfdd6bgUKvGFwovWrQoPcHMapAdd9zR2IX33nuvyx4zeRdbnNzuFXNccqqb4AvTZenSpemFyr6433///aYbLO/n3QxuxCfNJZ7gC+fWm29W/YcMVTX7HqlWOiPR/XR3dOXqFarlwdtN2PIed6c9q5tYXKGm/5tWjBdZ12iq4ehjjjdL5bIh8XIXcgvy7byTd/viDff9eZZa2JLZNYEw7rnnnvbKj+R5PI/iiD/BV7aKgQ537RdCEOHoCVIQfPjeV4jf3sTHJf7+fO/23ZfvI7/chVxyzS8tYByEkfscl9g890WT/vEw+oJc31Ys4ulcTMqnO2omghMQVAuJ2FOT63kfPn8YF9/9npg4Pj8YIdc9ETRx51p+fYj7orcjAXSf6Usj+O4VyyRJeQihFj6GvtFBOBI/0PdIQetugeurgro2UyYtYbVa9bc5qmnuS9F/GSbYIgYC5UZ5CKHWF1d29FfN/zldNc2356oEQQysI5RNd7SicrWxNv91WhDEwDpF+QzMaGRUjcngSEcsPoygiSklxYxDOXxPNvLFTe6Xa/z7grISQhkK57d55lRz3mVmnWLviWd0qTLefW9v4sCzGOYbmfMqxfdIHHy47j5/+a77EolfKeJQVkII5lh85vxUizlwtnbB1/ZOcWAFPpOw7kp8wc0In8nnJ34/Fyxe5qxNiD/jXmdzd+GEM3dNqusvnxFcu5DLTZ73VQByj0l4jovAZDsikuVq5AdhgDwr4fnsronj84Nx7wnufcqDHP8ofuK/SVF2QijQNUUQ2/48NeqaFqk1ZGsLGzTjZ8L4Ejo+7C5+sg3Hx8PgOlsGcqxD/LBbyObfF7bAkRWyQNz3fK7pA/zLM2IvxE2gApg5c6a9ysB6UPblcYbOnDlz0qeTu89CfX29yY8NNuj6Vwiu3/hzwHfh7po48u3uPZ/fhx56qNNJeHI//psEZSeEdEfdlS+czP2P2dNV3dIl2lE3kb0URllg7Z61IgnMynxqbc5D4WRrcecvzuScFNYx4o5f9pxxiC2/1PTsp2OlPfdxc08SywWt4jvvvGOWtfEsYRIe72OLlLQShElcpHXxLXoGCUdafAmTZwmDcHkn1xJX2bF+2mmnmedwl2V2vJ9WmzSRONFqcM0uB5aTET7PYKC2IRIqlqXxbL6F7YRLXElDeQ/x4D0YwiDO8u3Ek3dxeBbH+mPHD2Hghp00xJ2Kgmsg/3ged/k+OYiYc055N/AMccBgB/m2oqMLeyoJo4NOad0uVTdxmi6xT6TUlMeym6sfTjWO2Nv4d8Pgumq9KJyaxs1TdZNndQ5L2xsmTUv7453u83HD/XHjxum0TKV0Jqb988s1aCFKaQEzdt2dMnadaSmduSmdQcZdZ3LargtNSmdSShcQc4272HWGe+OEmxYA48e1Ew7vBy0Y5v2gBdT40wUipQUgpYUvpQuVucad+8QRuy5Qxp1ndeE0z+Mu/nXBNP6B9xJH4JfvAsLgHUB8tBAaO+ERLuFg18JhwiVOpCvvke/DyHcRN8IQdzcdiA+QvvJOLRxpd8IYOXKksUuaEB7pzvO4cY07dsCdOEm6EUfuEV/izrdKfpO/8n34193rdLq7+ep+X7FNYi2hFgxDpVhowbIZzUrOk4lBqyh/ULlCt4g1z/xJNa1Yqp/pfYsYhz8uAf6UhRPPqLmPOOIIs3p+woQJ6vDDD1dbb7218eOemkZLcM45mSMaOf1Mju0bOjT6h2Kdh+Y3F7yPcNj8CxwPyOFC7LZwz/LkMFz+B+PGG280OwloJVzYRMsGVRYms7gbpCVllwfnZ8puA/6Lgl3kwC6P8ePHG/u3335rFoGzPWjvvTN/FnrLLbeYOBE+XUjOXGWXAcfOx09SQ1fkWb7rl19+MTvotSCYViVfehBHDGG7fwVAK8X7OQ+WYx7paXB2qaSV5AMtIH88w655Wni6/eycJ76cIcq3jhkzxvhlgbns7mc3Bi03G3rjLfdRRx1lbcWnD7ujCFkuk33hr64tjFAv/PAt3TecYf5ExlCEReFugSDh6coMHz5cvfrqq+bkZbov6AsIJ//M44J/X5fT51aIIAL/NlQI0p32bbECvoOCylku8t8MsoVHtjbFt0fJcYtDhgwxBxuz84NCKafE5dtORT7Jd8p/RfAMzyOACAw71/ORbYtRtviKPkw3GXieriuV1C23R/oqlSk7TKjUSJtcZ/dIOlEOSAfSsNgnvbkkIoRkhOyIqZz/jvmvenY75DKVrdH+M3ebjEt66kILYtuf/pDREbuJHLtH60JtTcZQY5Ip3OMXN/SRiy+aZLbFsLOb1i1+xKGElQ+24VCjFyqIueAka5DjF2SLksA/FDHoxLk1kydP9v6dmw+EmQEUoCXk/BfS4rHHHjNu2aDAitC434c7aUxLzf92IICkYzFPsubbCPOYY6IzauUPZB588EHT6pMWpJP8RQFnvAKnubn/eCVQaQE79clbDi4mHSgLvsGnouH2TYtpdNA9Mr6w4gYdsGGT7VL1U2YbnZDn0Al9fl2DP9EFXHQim3uiwwB6hhZGo6sI6CGAviI6oW71zLNyjR03IDyugbCwSzzkXdjRgUS/Q28BdBWueafEDz/YBdE50XfkefQn1w+6G+7oSKJHiZ7Du7gGdD/s6EECYaKnSRqIXkT46FnYRX+W94shbDceoofKe8SI7kfYoouJ/kiYfJvohPjFHTfRgfGLP0HSDSPfKc/pijAdJ9IDJH3IA9818Ay6Iu7xMlUMk5gQZjPF+BAZhKkbMjRVN2xUemDG59c1PFOIIbPibiJs3TVSgOT57sQjbihsUoglDbIZ3pfPTy7jS4NsRutdxmCPfx/huPHoaTrmM25886VxNr+4u9fY3fhynZAxL10jka6rdH0L+W9DLQ/WVjx0GmYNF52SrhhdXPwJhcTDDVcXZHNKGeet5BokyBUXyHU/37OQ73mhkHAgn784hcYRXH+FPCf4/EqYSaDDXnOFUCDBkkyknlKsjJRw0C1lBA/K8ZsD3WetEMJyRgSotwLjCnQQvrWLIISBQIkp27WjgcC6QhDCQKDEBCEMBEpMEMJAoMQEIQwESkwQwkCgxAQhDARKTBDCQKDEBCEMBEqKUv8PAvIUv2lkl54AAAAASUVORK5CYII="
              />
            <span className="sr-only">INTERNNEXUS</span>
          </a>
          <div className="social" id="headerSocial">
            <ul>
              <li>
                <a href="">
                  {/* Twitter token */}
                  <img className="icoSocial" src="" />
                  {/* Twitter img token */}
                  <span className="sr-only">Twitter</span>
                </a>
              </li>
              <li>
                <a href="">
                  {/* LinkedIn token */}
                  <img className="icoSocial" src="" />
                  {/* LinkedIn img token */}
                  <span className="sr-only">LinkedIn</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div id="headerCover" className="row headerLeft">
          {" "}
          {/* Boolean variable adds or removes this class to position header left or right */}
          <div id="headerHero" className="col-sm-6">
            <div id="heroWrapper">
              <img
                className="media"
                data-object-fit="cover"
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA51BMVEUBAQEAAAD///8Ag60FAABxcXHS0tIEAwANbo4AhLAPc45lZWXGxsYAhKrMzMzAwMC6urra2trv7+9BQUFWVlaNjY2fn58XFxfg4ODr6+uWlpYOg6T4+PgsLCzl5eWvr695eXkLMkIUDgsdHR2kpKRMTEwPWWsUfaSRkZFGRkYNeJokJCRra2s4ODiEhIQGCxJeXl4PWnUQKzwULDYGi6wOIS0RSloUPkwHEBsJAA4NJS4RdowFFBoZWnQQgJwTNUQEY3UMKS0TPlMPGCgIABQYZ4EUUm8WYIU1My4UW3IAFRMPKT4NHCLVgYM8AAAJiklEQVR4nO2aCXvaOBqA+WRh44A5jAEbbCAchRAgSZNmm8xs05nZdDYz8/9/z346fHA0AZrQffJ8bw8bW7L0SrIuyOUIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgjgyoPnZ+XgzOFwjpx/v+M/OyRsBcHvjOM6NUfgE1s/OzBvAOQoaSMcxCvxd1iLnHccxJPa/3vhdBI4Fyo/cUCx+6sSGxuPbGnL4/OnTp7vjdmlgwWNi6Ez4myYO9zOn0/nl1+N22sALhqPA4/XbGhZEMd78G3JHfd95wYkNDfvhLQ3h3rFlS7ng1jGrEetwgm1H8GX2gO8I1+Reu6Th9Maw8Y9xLTubo0lCocBTLA7xKUBs+EpzHniYXV1dff369R74MSdRUHBOrFgPu9bbD5rbOAMAY8/zLs82crRnNuFh8psYLThYv0sujqPIC7ZdUBM2YQgzw7ElJ/H4D+AzpPwdw52zCadO5xTLEZPBRBy7c5xOFftSu+NcyRYJOUx9pgcPNNT9AUBNGJbWXSCqIb19DJ3JR3wHITfDt9HuHKcOhaHhGE8q+8rQ2NVQVe3OSWEdOpN7EIbGkQ2xzXwAMdq/sSEmZJ9cYEKT4xsazi2/eNEwRV4+16+n/girZ2soQ0yqkONWaph5l9OAK/G2PPcAQ0y48wdGfd5wWNTURTLDYl9crnlz+RH6o1Hbk2ceBvHEAepzFeHyXGRVGTqFO4hbKbJojEaNhbjdmqt4Mn7+PCcjYjmKy2MZVD0M0zvE0LBnn/kLdXjGYrpzHEKST01MvaFOfcyrDF4bB4zBKAlTxmI41RP8wn/ingasirpdFQayy45gIR8ErRAPAcClbkCLWpreQYaGcyIUnzOss5R+xrANMI3P3SGAKY6YvxCaaYRwkRjaX+NWKoNKKqBdeqpYitBypSh46vbQzaR3kCEOEoUL2NnQXanDRXqjkmZ7xRDja0PHvrG14TS9nUfFsogUibozYd2wlwY9tA4N237Eic1Lhm5FCdQXpbL6XO6r5LslkTdsZknFKMNapSsOQVKHGuczBOKGKcOjk3oNZGBvw1DNOkoIpnegIVbj43ZDM2OIfad8d/BNbKnLSFfWKhTFhX5s6FaU4RJAtrB1w86drHp8BWUBtfA51aS+NwxFEt1DOtMVQ8P49qKhqV86zHc8HqphQ3QWgWxDpm5LoAwb+gGDFUMcoKCvi0pKjOMHiQYiutYtdegGQVCNDhst4nR/38GwrfOdGMpXMgzFX9FVmLFgYlhODeMdBceYqD4lDGU07Fty+tH4iE3DuHpVEz7Y0LG/THY0zNahxzJoQ1HSWw2TOpxkew9tmGf62RuG6VAlGusPGIrNjB8zrD5vaMz+dBx7qyEOj2pICAebhnCZDheXhxtm2N1Q94FBVVJZPm9o/3J3Zes6VF2tjiemLfEMoRf3NG5qCIN5Y9owVdrHNAyiqCU7CDfCSZ81sOAFwy+cXznKsK8yb4l4EJeURLxqss6WraIKNIzOh62WKoPGsQzlvIqJTqWbNjb/BUNjdsfh8QYbaufuv5lG2tbTPVbTT1GDpWZlxGd7DYg/YgilOHOZuclLhs5MfJPwhO87zmlqabwpwFIcQzV4LmHFacUwHB7JEBeIoTbMpZW4g6HYqvnTFrO2TBc1hYFU66lIaDHMtIwVw+UP9KXfMZRFXVavSS0Z5fDOWN7BoS9KZmpdHVwajnTtyLutU7GVaM/EPiK/uHImOPOeJ02xraZKGC/STjCsJFKldDwM+ofPab5reJlHPBiKA67Y6uJYV4vSMZ6eiV5CXkSKOnhLGJ6pkOoK/+fp6urq6YM05Pyb85cYHzwdrw46ek6Fzou+Z6FvYuLj5OzAFfAzhuuL9uwu4jML++xtHU9sk3K9DcvhwYr3Tbfzvd2CPfc+Vmfe2w1fC1jZ5947q4emmho6j3bnTQ1/Dtn14fW3TC2+R0Pjb/6UKr5HQ+cjzyi+T0Ps2x63Gr7cK2wJsGNPAmmPvXOu92G1DnP8rnCzvnqKO/vns7mxIw718bNxsuPNTmkcWAZrrdTidydbDL2ulx30cpmjDtH20xs59c8MITui6e9E9X+6AHDN67pyIg3z7lhHTm2TZOSpJzeO9/1qdd0Q+OdHZ8OwyObpuJ11TLLSYxsDc9mFjbAZzK545IBVR0wa9HFhm33qxgnIZePLVf2sYU58yX0xs501w0u1x7B0Q5yD9sKwAUuzYvZrI2a2EsN2ucoq/TAYg9lz3bk0rDKs/UrPDNs9Zg6g2GVVGPvNMFg0w1Asa4esvwy1oefVmmF3sfDbrluE8xLzvXO/D1MfGi7GqzBMfRqG04PnpcIQZ1MW/yw33TcNi2y0ZPml6zXZeZPVpm3WbOvVqDDssSka53GKHgZzPxSGlTBfYuCH/TLrNVjjnFXmbDRmpXxYWvjBpSi1HnPHseGcVfusF+FD3BqYbt5kEJTBNxdho8iWY1au5xmmutcmBlbZiR3/GONUzRgBrie2s8WwytpTLHhvVGHjJhOrKNVyYsMQlwV9MH0IRzBnCzRkNay7sV+GBjsH1l6yUbvrj3EFX/Oh7IsWWA/CLphT0IZ1VIpYHspBC1cbddZvhwNcLUX4mKZIqxyi4e5fysaGMac6JofrmWOsG2KTC6vVaqPB2m00DFvCsCXfjcRwkRgWWV0aVivVum/iEjlCwymrVKpNDytBGWJhlgJwTbFoV4ZjadhHw3NsHRFbRgxf0zNRt9LQDPGB+23TWPy2cKK5j399wfmvs7Wepl8/i5ZYmk2r5GKNYB0mhpe45F4zNPGlEa3Ud6NFG2LD5hkbwXKsDc1AdkdsURT7TRuG4Pr40Ah8bPVLtqgLwwomOYfmXkv8tX4urdo/OifpbzHVdn1XrLPdM09spfSFYVMZsjNlWBWGS6h1IeyKFTKOFlFXLF6DGuZsAag3xcX7XBh2fcw29jCwwBBlvAOiL82jYWjKpu7COJSr+TZ+wtV+IBfBciHc3e/7Q62wfsGCf+7THxGmRTAU/7VWSiVC17WCwlZq6dOWtXprmJ4OBjLe0Fp/YiZsbuV0KEbG1mvNfcTPQtIS2Ia+2qiurIDxzEoHrtXj+hiXibeaxurl3NqN1xHMbanY7aE2kgS4rL+rH41vyrxmMRMEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRAEQRDE/z3/AzqOzipMCV3RAAAAAElFTkSuQmCC"
              />
            </div>
          </div>
          <div id="headerMasthead" className="col-sm-6">
            <div id="mastheadWrapper">
              <div id="mastheadContent">
                <div className="verticalCenter">
                  <h4
                    id="mastheadContentType"
                    className="mktoText"
                    mktoname="Header label"
                  >
                    Webinar
                  </h4>
                  <h1
                    id="mastheadHeadline"
                    className="mktoText"
                    mktoname="Headline"
                  >
                    Web Development Fundamentals: Learn the basics of HTML, CSS,
                    and JavaScript to build your own websites. .
                  </h1>
                  <p
                    id="mastheadSubheadline"
                    className="mktoText"
                    mktoname="Subhead"
                  >
                    Wednesday, December 13, 2017
                    <br />
                    12 p.m. CT
                  </p>
                  <a href="#formSection" id="mastheadCTA" className="scrollLink">
                    call to action
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="clearfix" />
        <div id="headerCover" className="row headerRight">
          {" "}
          {/* Boolean variable adds or removes this class to position header left or right */}
          <div id="headerHero" className="col-sm-6">
            <div id="heroWrapper">
              <img
                className="media"
                data-object-fit="cover"
                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT26L0wakNwa0MJLoITT-EGA1f-VDtUr6nc1g&usqp=CAU"
              />
            </div>
          </div>
          <div id="headerMasthead" className="col-sm-6">
            <div id="mastheadWrapper">
              <div id="mastheadContent">
                <div className="verticalCenter">
                  <h4
                    id="mastheadContentType"
                    className="mktoText"
                    mktoname="Header label"
                  >
                    Webinar
                  </h4>
                  <h1
                    id="mastheadHeadline"
                    className="mktoText"
                    mktoname="Headline"
                  >
                    From zero experience to your Dream job Helping students and
                    recent graduates start their career. Enabling skills
                    development in a live environment, making students industry
                    ready.
                  </h1>
                  <p
                    id="mastheadSubheadline"
                    className="mktoText"
                    mktoname="Subhead"
                  >
                    Wednesday, December 13, 2017
                    <br />
                    12 p.m. CT
                  </p>
                  <a href="#formSection" id="mastheadCTA" className="scrollLink">
                    Call to action
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
      <section className="container" id="webinarInfo">
        <div className="row">
          <div
            className="col-sm-6 mktoText"
            id="webinarOverview"
            mktoname="Overview text"
          >
            <h3>
              Let InternNexus host your webinars. Offer seamless support
              regardless of location or time constraints with the help of remote
              tools and technologies that enable efficient communication and
              collaboration.{" "}
            </h3>
          </div>
          <div
            className="col-sm-6 mktoText"
            id="webinarDetails"
            mktoname="Details text"
          >
            <p>
              Alight Solutions’ top{" "}
              <a href="">retirement and healthcare experts</a>{" "}
              <strong>Rob Austin</strong>, <strong>John Compton</strong> and{" "}
              <strong>Ileana Torres</strong> will share data and insights during
              our upcoming webinar,{" "}
              <strong>
                <i>HSAs and 401(k)s: Breaking the silos</i>
              </strong>
              .
            </p>
            <p>
              <strong>We will discuss:</strong>
            </p>
            <ul>
              <li>Employee savings trends across HSAs and 401(k)s</li>
              <li>How offering both HSAs and 401(k)s impacts contributions</li>
              <li>Best practices to increase participation in HSAs</li>
            </ul>
          </div>
        </div>
      </section>
      <section className="container" id="webinarExperts">
        <div className="separator row">
          <div className="col-sm-12">
            <hr className="hrArc" />
          </div>
        </div>
        <div className="speakers row">
          <div className="col-sm-12">
            <h3
              className="mktoText"
              id="eventSpeakers"
              mktoname="Title for speakers module"
            >
              Get insights from top experts
            </h3>
          </div>
          <div className="speakerGrid">
            <div className="speaker">
              <div
                className="mktoImg"
                id="speaker1Img"
                mktoname="Speaker 1 headshot"
              >
                <img src="assets/img/placeholder-speaker.png" />
              </div>
              <div className="credentials">
                <h3
                  className="secondary mktoText"
                  id="speaker1Name"
                  mktoname="Name of speaker 1"
                >
                  Speaker 1
                </h3>
                <p
                  className="bio mktoText"
                  id="speaker1Title"
                  mktoname="Title of speaker 1"
                >
                  Speaker 1 Title
                  <br />
                  <a href="">LinkedIn profile</a>
                </p>
              </div>
            </div>
            <div className="speaker">
              <div
                className="mktoImg"
                id="speaker2Img"
                mktoname="Speaker 2 headshot"
              >
                <img src="assets/img/placeholder-speaker.png" />
              </div>
              <div className="credentials">
                <h3
                  className="secondary mktoText"
                  id="speaker2Name"
                  mktoname="Name of speaker 2"
                >
                  Speaker 2
                </h3>
                <p
                  className="bio mktoText"
                  id="speaker2Title"
                  mktoname="Title of speaker 2"
                >
                  Speaker 2 title
                </p>
              </div>
            </div>
            <div className="speaker">
              <div
                className="mktoImg"
                id="speaker3Img"
                mktoname="Speaker 3 headshot"
              >
                <img src="assets/img/placeholder-speaker.png" />
              </div>
              <div className="credentials">
                <h3
                  className="secondary mktoText"
                  id="speaker3Name"
                  mktoname="Name of speaker 3"
                >
                  Speaker 3
                </h3>
                <p
                  className="bio mktoText"
                  id="speaker3Title"
                  mktoname="Title of speaker 3"
                >
                  Speaker 3 title
                </p>
              </div>
            </div>
            <div className="speaker">
              <div
                className="mktoImg"
                id="speaker4Img"
                mktoname="Speaker 4 headshot"
              >
                <img src="assets/img/placeholder-speaker.png" />
              </div>
              <div className="credentials">
                <h3
                  className="secondary mktoText"
                  id="speaker4Name"
                  mktoname="Name of speaker 4"
                >
                  Speaker 4
                </h3>
                <p
                  className="bio mktoText"
                  id="speaker4Title"
                  mktoname="Title of speaker 4"
                >
                  Speaker 4 title
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="fullBleed" id="formSection">
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <div className="webinarDetails">
                {" "}
                {/* Boolean switches ID between webinarUpcoming to webinarOnDemand */}
                <h2
                  className="mktoText"
                  id="webinarTitle"
                  mktoname="Webinar title"
                >
                  Webinar title goes here
                </h2>
                <p
                  className="mktoText"
                  id="webinarDateTime"
                  mktoname="Webinar details"
                >
                  <strong>Date:</strong> Tuesday, January 16, 2018
                  <br />
                  <strong>Time:</strong> 12:00pm-1:00pm CT
                </p>
              </div>
            </div>
            <div className="col-sm-6" id="registrationModule">
              <div id="registrationSuccess">
                <div id="confirmForm">
                  <h3>Thank you!</h3>
                  <h3 className="confirm">Lorem ipsum dolor sit amet</h3>
                  {/*  <hr class="shortThick">  */}
                  <div className="primaryCTAbutton">
                    <a href="" id="registrationConfirmCTA" target="_blank">
                      call to action
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer className="fullBleed" id="globalFooter">
        <div className="container">
          <div className="row">
            <div className="col-sm-6" id="contactModule">
              <div className="vertCenter">
                <h3 className="contactHeader">Contact Us</h3>
                <h3 className="contactText">
                  Looking for more information or have questions?
                  <br />
                  Let us know and we’ll be in touch.
                </h3>
                <a href="" id="contactCTA" className="btn">
                  Talk to an expert
                </a>
              </div>
            </div>
          </div>
        </div>
        <div id="footerArcWrapper">
          <div id="footerArcIEFix">
            {/* Do I need this for an absolutely positioned fixed size SVG? */}
            <svg
              id="footerArc"
              data-name="Arc"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 600 300"
            >
              <title>Arc</title>
              <path
                className="arcSVG"
                fill="#231f20"
                d="M300,131C219.6,131,152,75.1,133.9,0H0C19.7,148.2,146.7,263,300,263S580.3,148.2,600,0H466.1C448,75.1,380.4,131,300,131"
              />
            </svg>
          </div>
        </div>
        <div className="footerLinksWrapper">
          <a href="" target="_blank">
            {/* Logo link token */}
            <svg
              className="logo"
              id="alightLogoSVG"
              data-name="INTERNEUXS"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              viewBox="0 0 328 132"
            >
              <title>INTERNEXUS</title>
            </svg>
            <span className="sr-only">Alight</span>
          </a>
          <ul className="list-inline">
            <li>
              <a href="">Legal</a>
            </li>
            <li>
              <a href="">Privacy</a>
            </li>
            <li>
              <a href="">Cookie Notice</a>
            </li>
          </ul>
          <p>
            ©2018 Alight Solutions.
            <br />
            All rights reserved.
          </p>
        </div>
      </footer>
      {/* /Start Global Footer Snippet */}
    </main>
  </div>
  
  );
}

export default App;
